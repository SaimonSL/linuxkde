module ApplicationHelper
  def error_display(object)
    template = ["<div class='alert alert-error'>"]
    template += ["<button type='button' class='close' data-dismiss='alert'>",
                 '&times;', '</button>']
    template += ['<h4>', pluralize(object.errors.count, "error"),
                 ' prohibited this software from being saved:', '</h4>' ]

    template += ['<ul>']
    object.errors.full_messages.each do |msg|
      template += ['<li>', msg, '</li>']
    end
    template += ['</ul>']
    template += ['</div>']
    template.join().html_safe
  end
end
