module ButtonHelper
  def add_button(name, url)
    haml_tag :a, href: url, class: 'btn btn-success button' do
      haml_tag :i, class: 'fa fa-plus'
      haml_concat "Add New #{name}"
    end
  end

  def edit_button(name, url)
    haml_tag :a, href: url, class: 'btn btn-info button' do
      haml_tag :i, class: 'fa fa-edit'
      haml_concat "Edit #{name}"
    end
  end

  def delete_button(name, url)
    haml_tag :a, href: url, class: 'btn btn-danger button',
      data: { confirm: "Are you sure you want to delete #{name} ?",
              method: :delete }, rel: :nofollow  do
      haml_tag :i, class: 'fa fa-trash-o'
      haml_concat "Delete #{name}"
    end
  end

  def back_button(label, url)
    haml_tag :a, href: url, class: 'btn button-black' do
      haml_tag :i, class: 'fa fa-mail-reply'
      haml_concat label
    end
  end

  def show_button(name, url)
    haml_tag :a, href: url, class: 'btn button-black' do
      haml_tag :i, class: 'fa fa-eye-open'
      haml_concat "Show #{name}"
    end
  end

  def list_button(name, url)
    haml_tag :a, href: url, class: 'btn button btn-inverse' do
      haml_tag :i, class: 'fa fa-list'
      haml_concat "List #{name}"
    end
  end
end
