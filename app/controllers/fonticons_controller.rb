class FonticonsController < ApplicationController
  before_action :set_fonticon, only: [:edit, :update, :destroy]
  before_action :log_visitors
  before_action :secure_section

  def index
    @fonticons = Fonticon.all
  end

  def new
    @fonticon = Fonticon.new
  end

  def edit
  end

  def create
    @fonticon = Fonticon.new(fonticon_params)

    if @fonticon.save
      redirect_to fonticons_path,
        notice: 'Fonticon was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @fonticon.update(fonticon_params)
      redirect_to fonticons_path,
        notice: 'Fonticon was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @fonticon.destroy
    redirect_to fonticons_path, notice: 'Fonticon was successfully destroyed.'
  end

  private
    def set_fonticon
      @fonticon = Fonticon.find(params[:id])
    end

    def fonticon_params
      params.require(:fonticon).permit(:icon)
    end
end
