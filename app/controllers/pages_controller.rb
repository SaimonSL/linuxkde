class PagesController < ApplicationController
  before_action :find_section
  before_action :find_software
  before_action :find_page, only: [:show, :edit, :update, :destroy]
  before_action :log_visitors
  before_action :secure_section, except: [:show]
  before_action :validate_software

  def index
    @pages = Page.all
  end

  def show
    if is_admin?
      @pages = @software.pages
    else
      redirect_to section_software_path(@section, @software),
        notice: 'This page is not ready at the moment.'
    end
  end

  def new
    @page = Page.new(position: @software.next_page_position)
  end

  def edit
  end

  def create
    @page = Page.new(page_params)
    @page.software_id = @software.id

    if @page.save
      redirect_to section_software_page_path(@section, @software, @page),
        notice: 'Page was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @page.update(page_params)
      redirect_to [@section, @software, @page], notice: 'Page was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def sort
    unless params[:position].blank?
      params[:position].each_with_index do |id, index|
        Page.set_position(id, index)
      end
    end
    render json: params[:position].to_json
  end

  def destroy
    @page.destroy
    redirect_to section_software_path(@section, @software),
      notice: 'Page was successfully destroyed.'
  end

  private
    def page_params
      params.require(:page).permit(:position, :name, :enabled)
    end

    def validate_software
      if !@software.instance_of?(Software)
        redirect_to section_softwares_url(@section)
      end
    end
end
