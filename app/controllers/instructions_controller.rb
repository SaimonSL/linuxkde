class InstructionsController < ApplicationController
  before_action :find_section
  before_action :find_software
  before_action :find_page
  before_action :find_instruction, only: [:show, :edit, :update, :destroy, :destroyimage]
  before_action :secure_section
  before_action :validate_page

  def index
    @instructions = @page.instructions_by_position
  end

  def new
    @instruction = Instruction.new(position: @page.next_instruction_position)
  end

  def edit
  end

  def create
    @instruction = Instruction.new(instruction_params)
    @instruction.page = @page

    if @instruction.save
      redirect_to [@section, @software, @page],
        notice: 'Instruction was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @instruction.update(instruction_params)
      redirect_to [@section, @software, @page],
        notice: 'Instruction was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def sort
    unless params[:position].blank?
      params[:position].each_with_index do |id, index|
        Instruction.set_position(id, index)
      end
    end
    render json: params[:position].to_json
  end

  def destroy
    @instruction.destroy
    redirect_to section_software_page_path(@section, @software, @page),
      notice: 'Instruction was successfully destroyed.'
  end

  def destroyimage
    @instruction.picture.destroy
    @instruction.save
    redirect_to [@section, @software, @page],
      notice: 'Instruction image was successfully removed.'
  end

  private
    def instruction_params
      params.require(:instruction).permit(:page_id, :position, :info, :picture)
    end

    def validate_page
      if !@page.instance_of?(Page)
        redirect_to section_software_path(@section, @software)
      end
    end
end
