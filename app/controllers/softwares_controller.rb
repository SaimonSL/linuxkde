class SoftwaresController < ApplicationController
  before_action :find_section
  before_action :find_software, only: [:show, :edit, :update, :destroy]
  before_action :log_visitors
  before_action :secure_section, except: [:show]
  before_action :validate_section

  def index
    @softwares = Software.all
  end

  def show
    if is_admin?
      @pages = @software.pages_by_position
    else
      if @software.enabled
        @page = @software.pages_by_position.by_enabled
      else
        redirect_to @section, notice: 'This software is not ready.'
      end
    end
  end

  def new
    @software = Software.new
  end

  def edit
  end

  def create
    @software = Software.new(software_params.merge(section_id: @section.to_param))

    if @software.save
      redirect_to [@section, @software], notice: 'Software was successfully created.'
    else
      render action: 'new'
    end
  end

  def update
    if @software.update(software_params)
      redirect_to [@section, @software], notice: 'Software was successfully updated.'
    else
      render action: 'edit'
    end
  end

  def destroy
    @software.destroy
    redirect_to section_path(@section), notice: 'Software was successfully destroyed.'
  end

  private
    def software_params
      params.require(:software).permit(:icon,
                                       :name,
                                       :description,
                                       :enabled,
                                       :tags)
    end

    def validate_section
      redirect_to sections_url if !@section.instance_of?(Section)
    end
end
