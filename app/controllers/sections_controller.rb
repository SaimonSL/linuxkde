class SectionsController < ApplicationController
  before_action :find_section, only: [:show, :edit, :update, :destroy]
  before_action :log_visitors
  before_action :secure_section, except: [:index, :show, :about]

  def index
  end

  def list
  end

  def show
    redirect_to sections_url if !@section.instance_of?(Section)
  end

  def new
    @section = Section.new(position: Section.next_section_position)
  end

  def edit
  end

  def create
    @section = Section.new(section_params)

    respond_to do |format|
      if @section.save
        format.html { redirect_to @section, notice: 'Section was successfully created.' }
        format.json { render json: { status: :created, location: @section } }
      else
        format.html { render action: 'new' }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @section.update(section_params)
        format.html { redirect_to @section, notice: 'Section was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @section.errors, status: :unprocessable_entity }
      end
    end
  end

  def sort
    unless params[:position].blank?
      params[:position].each_with_index do |id, index|
        Section.set_position(id, index)
      end
    end
    render json: params[:position].to_json
  end

  def destroy
    @section.destroy
    respond_to do |format|
      format.html { redirect_to sections_url }
      format.json { head :no_content }
    end
  end

  def about
  end

  private
    def section_params
      params.require(:section).permit(:icon,:name,:position,:description,:tags)
    end
end
