class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_filter :load_sections

  helper_method :is_admin?
  def is_admin?
    # TODO: Depends on if I will manage the site myself v.s admins this may
    # needs to be done using device with cancancan
    ['127.0.0.1','24.184.103.174'].include? request.remote_ip
  end

  def secure_section
    unless is_admin?
      case params[:controller]
        when 'fonticons'
          redirect_to sections_path,
            notice: 'You are not authorized to be here.'
        when 'sections'
          redirect_to sections_path,
            notice: 'You are not authorized to be here.'
        when 'softwares'
          redirect_to @section,
            notice: 'You are not authorized to be here.'
        when 'pages'
          redirect_to [@section, @software],
            notice: 'You are not authorized to be here.'
        when 'instructions'
          redirect_to [@section, @software],
            notice: 'You are not authorized to be here.'
      end
    end
  end

  def load_sections
    @sections = Section.all.by_position
  end

  def log_visitors
    unless is_admin?
      visitor = Visitor.new(ip: request.remote_ip)
      visitor.section_id = @section.id if !@section.blank?
      visitor.software_id = @software.id if !@software.blank?
      visitor.page_id = @page.id if !@page.blank?
      visitor.save
    end
  end

  def find_section
    @section = Section.find(params[:section_id])  if params[:section_id]
    @section = Section.find(params[:id])          if @section.blank?
  end

  def find_software
    @software = Software.find(params[:software_id]) if params[:software_id]
    @software = Software.find(params[:id])          if @software.blank?
  end

  def find_page
    @page = Page.find(params[:page_id])   if params[:page_id]
    @page = Page.find(params[:id])        if @page.blank?
  end

  def find_instruction
    @instruction = Instruction.find(params[:instruction_id]) if params[:instruction_id]
    @instruction = Instruction.find(params[:id]) if @instruction.blank?
  end
end