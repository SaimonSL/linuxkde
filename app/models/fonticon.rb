class Fonticon < ActiveRecord::Base
  validates_presence_of :icon
  validates_uniqueness_of :icon

  default_scope { order('icon') }

  def to_param
    "#{id}-#{icon.try(:parameterize)}"
  end
end
