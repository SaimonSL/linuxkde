class Visitor < ActiveRecord::Base

  validates_presence_of :ip

  belongs_to :section
  belongs_to :software
  belongs_to :page
end
