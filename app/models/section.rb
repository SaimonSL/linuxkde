class Section < ActiveRecord::Base
  validates_presence_of :name
  validates_presence_of :icon
  validates_presence_of :position
  validates_presence_of :description
  validates_presence_of :tags

  validates_uniqueness_of :name
  validates_uniqueness_of :icon
  validates_uniqueness_of :tags

  has_many :visitors
  has_many :softwares, dependent: :destroy

  scope :by_position, -> { order(:position) }

  def to_param
    "#{id}-#{name.try(:parameterize)}"
  end

  def name_to_id
    name.parameterize
  end

  def self.set_position(find_id, new_position)
    Section.find(find_id).update_attribute(:position, new_position)
  end

  def self.next_section_position
    return 1 if (Section.count == 0)
    position = Section.all.order('position DESC').limit(1).first.position
    position+1
  end
end
