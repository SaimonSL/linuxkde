class Instruction < ActiveRecord::Base
  has_attached_file :picture, default_url: "/images/missing.jpg"
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\Z/
  validates_attachment :picture, size: { in: 1..900.kilobytes }

  validates_presence_of :page_id
  validates_presence_of :position
  validates_presence_of :info

  belongs_to :page

  def self.set_position(find_id, new_position)
    Instruction.find(find_id).update_attribute(:position, new_position)
  end
end
