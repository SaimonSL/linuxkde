class Page < ActiveRecord::Base
  belongs_to :software

  has_many :visitors
  has_many :instructions, dependent: :destroy

  validates_presence_of :software_id
  validates_presence_of :position
  validates_presence_of :name

  scope :by_enabled, -> { where(enabled: true) }

  def self.set_position(find_id, new_position)
    Page.find(find_id).update_attribute(:position, new_position)
  end

  def to_param
    "#{id}-#{name.try(:parameterize)}"
  end

  def instructions_by_position
    instructions.order('position ASC')
  end

  def next_instruction_position
    return 1 if (instructions.count == 0)
    position = instructions.order('position DESC').limit(1).first.position
    position+1
  end
end
