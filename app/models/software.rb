class Software < ActiveRecord::Base

  has_attached_file :icon,
    styles: { large: "200x200>",  medium: "128x128>", small: "64x64>" },
    default_url: "/images/:style/software.png"

  validates_attachment :icon, presence: true, size: { in: 1..500.kilobytes }
  validates_attachment_content_type :icon, content_type: /\Aimage\/.*\Z/

  validates_presence_of :section_id
  validates_presence_of :name
  validates_presence_of :description
  validates_presence_of :tags

  validates_uniqueness_of :name
  validates_uniqueness_of :tags

  belongs_to :section

  has_many :visitors
  has_many :pages, dependent: :destroy

  scope :by_name, -> { order(:name) }
  scope :by_enabled, -> { where(enabled: true) }

  def to_param
    "#{id}-#{name.try(:parameterize)}"
  end

  def name_to_id
    name.parameterize
  end

  def pages_by_position
    Page.where("software_id = ?", id)
    .order(:position)
  end

  def next_page_position
    return 1 if (pages.count == 0)
    position = pages.order('position DESC').limit(1).first.position
    position+1
  end
end
