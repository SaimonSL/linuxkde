jQuery ->
  if $('select#section_icon').length
    preview_icon()
    $('select#section_icon').change ->
      preview_icon()

  return

preview_icon = ->
  $('#preview-icon').removeClass().addClass('fa ' + $('select#section_icon').val())