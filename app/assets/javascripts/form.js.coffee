jQuery ->
  $('.make-switch').bootstrapSwitch()

  # Icon Selector Model
  $('#icon-selector-model .modal-body i').click ->
    set_selected_icon(this)
    target = $('#icon-selector-model').data('target')
    $(target).val(this.id)
    $(target).trigger('change')

  # Sortable List
  $("#sortable-items").sortable({
    handle: '.sort-mover'
    axis: 'y'
    scroll: true
    opacity: 0.75
    update: (event, ui)->
      itm_arr = $("#sortable-items").sortable('toArray')
      pobj = {position: itm_arr}
      $.post($(this).data('url'), pobj)
      if $(this).data('oncomplete') != 'undefined'
        eval($(this).data('oncomplete'))
  })

  return

set_selected_icon =(icon_obj)->
  $('#icon-selector-model .modal-body i').removeClass('selected-icon')
  $(icon_obj).addClass('selected-icon')
  return
