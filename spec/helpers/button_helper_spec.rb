require 'spec_helper'

describe ButtonHelper do

  before do
    helper.extend Haml
    helper.extend Haml::Helpers
  end

  describe 'with button generators it should' do
    it 'generate add button string' do
      helper.capture_haml { add_button('Section', 'myurl') }.should
      match /Add New Section/
    end

    it 'generate edit button string' do
      helper.capture_haml { edit_button('Section', 'myurl') }.should
      match /Edit Section/
    end

    it 'generate delete button string' do
      helper.capture_haml { delete_button('Section', 'myurl') }.should
      match /Delete Section/
    end

    it 'generate back button string' do
      helper.capture_haml { back_button('Back To Section', 'myurl') }.should
      match /Back To Section/
    end

    it 'generate show button string' do
      helper.capture_haml { show_button('Section', 'myurl') }.should
      match /Show Section/
    end

    it 'generate list button string' do
      helper.capture_haml { list_button('Section', 'myurl') }.should
      match /List Section/
    end
   end
end
