FactoryGirl.define do
  factory :section do
    sequence(:name)       { |n| "Section #{n}" }
    sequence(:icon)       { |n| "fa fa-#{n}" }
    sequence(:position)   { |n| "#{n}" }
    sequence(:tags)       { |n| "tag #{n}" }
    description 'Internet section'
  end
end
