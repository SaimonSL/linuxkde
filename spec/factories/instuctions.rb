FactoryGirl.define do
  factory :instruction do
    page_id 1
    position 1
    info 'Welcome'
    picture { File.new(Rails.root.join('spec', 'fixtures', 'sample.png')) }
  end
end
