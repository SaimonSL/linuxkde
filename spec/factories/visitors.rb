FactoryGirl.define do
  factory :visitor do
    ip '127.0.0.1'
    section_id 1
    software_id 2
    page_id 3
  end
end
