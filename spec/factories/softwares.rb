FactoryGirl.define do
  factory :software do
    sequence(:section_id) { |n| n }
    sequence(:name) { |n| "Software #{n}" }
    description 'Image ediator'
    sequence(:tags) { |n| "tag #{n}" }
    icon { File.new(Rails.root.join('spec', 'fixtures', 'sample.png')) }
  end
end
