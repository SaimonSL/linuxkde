FactoryGirl.define do
  factory :page do
    software_id 1
    sequence(:position)   { |n| "#{n}" }
    sequence(:name)       { |n| "Introduction #{n}" }
    enabled true
  end
end
