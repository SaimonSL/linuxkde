require 'spec_helper'

describe Instruction do
  stub_paperclip Instruction

  it { should validate_presence_of(:page_id) }
  it { should validate_presence_of(:position) }
  it { should validate_presence_of(:info) }

  it { should have_attached_file(:picture) }
  it { should validate_attachment_content_type(:picture).
     allowing('image/png', 'image/gif', 'image/jpeg').rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:picture).less_than(900.kilobytes) }

  describe 'with self functions' do
    before do
      @instruction = FactoryGirl.create(:instruction, position: 2)
    end

    it 'set_position should set position of an instruction' do
      Instruction.set_position(@instruction.id, 99)
      Instruction.last.position.should eq(99)
    end
  end
end
