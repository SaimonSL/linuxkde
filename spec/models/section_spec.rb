require 'spec_helper'

describe Section do
  stub_paperclip Software

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:icon) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:tags) }
  
  it { should validate_uniqueness_of(:name) }
  it { should validate_uniqueness_of(:icon) }
  it { should validate_uniqueness_of(:tags) }
  
  describe 'with instance functions' do
    before do
      @section = FactoryGirl.create(:section)
    end

    it 'to_param sould return valid param' do
      @section.to_param.should
        eq("#{@section.id}-#{@section.name.try(:parameterize)}")
    end

    it 'name_to_id should return valid name html safe' do
      @section.name_to_id.should eq(@section.name.parameterize)
    end
  end
end
