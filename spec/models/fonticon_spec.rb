require 'spec_helper'

describe Fonticon do
  it { should validate_presence_of(:icon) }
  it { should validate_uniqueness_of(:icon) }
end
