require 'spec_helper'

describe Page do
  stub_paperclip Software
  stub_paperclip Instruction

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:software_id) }
  it { should validate_presence_of(:position) }

  describe 'with instance functions' do
    before do
      @page = FactoryGirl.create(:page, position: 2)
    end

    it 'to_param sould return valid param' do
      @page.to_param.should
        eq("#{@page.id}-#{@page.name.try(:parameterize)}")
    end

    it 'instructions_by_position get instructions in order' do
      instruction = FactoryGirl.create(:instruction, page: @page, position: 2)
      FactoryGirl.create(:instruction, page: @page, position: 1)
      @page.instructions_by_position.last.should eq(instruction)
    end
  end

  describe 'with self functions' do
    before do
      @page = FactoryGirl.create(:page, position: 2)
    end

    it 'set_position should set position of a page' do
      Page.set_position(@page.id, 99)
      Page.last.position.should eq(99)
    end
  end

  describe 'with instructions handling' do
    before do
      @page = FactoryGirl.create(:page)
      @ins1 = FactoryGirl.create(:instruction, page: @page, position: 2)
      @ins2 = FactoryGirl.create(:instruction, page: @page, position: 1)
    end

    it 'instructions_by_position should return in order' do
      @page.instructions_by_position.first.should eq(@ins2)
    end

    it 'next_instruction_position should get next position' do
      @page.next_instruction_position.should eq(3)
    end

    it 'next_instruction_position should return 1 of no pages found' do
      Instruction.delete_all
      @page.next_instruction_position.should eq(1)
    end

    it 'next_instruction_position should only target valid instructions' do
      FactoryGirl.create(:instruction, page_id: 99, position: 3)
      @page.next_instruction_position.should eq(3)
    end
  end
end
