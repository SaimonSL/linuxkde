require 'spec_helper'

describe Software do
  stub_paperclip Software
  stub_paperclip Instruction

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:section_id) }
  it { should validate_presence_of(:description) }
  it { should validate_presence_of(:tags) }

  it { should validate_uniqueness_of(:name) }
  it { should validate_uniqueness_of(:tags) }

  it { should have_attached_file(:icon) }
  it { should validate_attachment_presence(:icon) }
  it { should validate_attachment_content_type(:icon).
     allowing('image/png', 'image/gif').rejecting('text/plain', 'text/xml') }
  it { should validate_attachment_size(:icon).less_than(500.kilobytes) }

  describe 'with instance functions' do
    before do
      @software = FactoryGirl.create(:software)
    end

    it 'to_param sould return valid param' do
      @software.to_param.should
        eq("#{@software.id}-#{@software.name.try(:parameterize)}")
    end

    it 'name_to_id should return valid name html safe' do
      @software.name_to_id.should eq(@software.name.parameterize)
    end
  end

  describe 'with pages handling' do
    before do
      @software = FactoryGirl.create(:software)
      @page1 = FactoryGirl.create(:page, software: @software, position: 1)
      @page2 = FactoryGirl.create(:page, software: @software, position: 2)
    end

    it 'get_pages_by_position should return in order' do
      @software.pages_by_position.first.should eq(@page1)
    end

    it 'next_page_position should get next position' do
      @software.next_page_position.should eq(3)
    end

    it 'next_page_position should return 1 of no pages found' do
      Page.delete_all
      @software.next_page_position.should eq(1)
    end

    it 'next_page_position should only target valid pages' do
      FactoryGirl.create(:page, software_id: 99, position: 3)
      @software.next_page_position.should eq(3)
    end
  end
end
