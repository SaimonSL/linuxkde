require 'spec_helper'
FactoryGirl.factories.map(&:name).each do |factory_name|
  describe "The #{factory_name} factory" do
    subject { FactoryGirl.build(factory_name.to_sym) }
    it { should be_valid }
  end
end
