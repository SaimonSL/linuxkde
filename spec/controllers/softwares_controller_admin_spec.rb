require 'spec_helper'

describe SoftwaresController do
  stub_paperclip Software
  stub_paperclip Instruction

  let(:asset_icon) { fixture_file_upload('/sample.png', 'image/png') }

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section, enabled: true)
    @disabled_software = FactoryGirl.create(:software, section: @section,
      name: 'Not ready!', enabled: false)
    @page = FactoryGirl.create(:page, software: @software)
    @disabeld_page = FactoryGirl.create(:page, software: @software,
      name: 'not ready!', enabled: false)
    @page_for_disabled = FactoryGirl.create(:page, software: @disabled_software)
  end

  describe 'as an Admin user and' do
    describe 'with INDEX' do
      before do
        get :index, section_id: @section.to_param
      end

      it 'it will render' do
        response.should be_success
      end

      it 'it will have softwares variable' do
        assigns(:softwares).should include(@software)
      end

      it 'it will show disabled software for admins' do
        get :index, section_id: @section.to_param
        assigns(:softwares).should include(@disabled_software)
      end

      it 'it will render without any softwares' do
        Software.delete_all
        get :index, section_id: @section.to_param
        response.should be_success
      end
    end

    describe 'with SHOW' do
      describe 'with enabled it should' do
        before do
          get :show, section_id: @section.to_param, id: @software.to_param
        end

        it 'be able to render' do
          response.should be_success
        end

        it 'have software variable' do
          assigns(:software).should eq(@software)
        end

        it 'have page' do
          assigns(:software).pages.should include(@page)
        end

        it 'have disabled page' do
          assigns(:software).pages.should include(@disabeld_page)
        end

        it 'pages should be in order' do
          Page.delete_all
          last = FactoryGirl.create(:page, position: 99, software: @software)
          first = FactoryGirl.create(:page, position: 90, software: @software)
          get :show, section_id: @section.to_param, id: @software.to_param
          assigns(:pages).first.should eq(first)
          assigns(:pages).last.should eq(last)
        end
      end

      describe 'with disabled it should' do
        before do
          get :show, section_id: @section.to_param, id: @disabled_software.to_param
        end

        it 'be able to render' do
          response.should be_success
        end

        it 'have software variable' do
          assigns(:software).should eq(@disabled_software)
        end

        it 'have page' do
          assigns(:software).pages.should include(@page_for_disabled)
        end

        it 'not have disabled page' do
          assigns(:software).pages.should_not include(@disabeld_page)
        end
      end
    end

    describe 'with NEW' do
      before do
        get :new, section_id: @section.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'create a new instance of software' do
        assigns(:software).should be_a_new(Software)
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, section_id: @section.to_param, id: @software.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'target the correct software' do
        assigns(:software).should eq(@software)
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { name: 'hello', description: 'X', tags: 'L', icon: asset_icon }
      end

      it 'create new software' do
        expect {
          post :create, section_id: @section.to_param, software: @params
        }.to change(Software, :count).by(1)
      end

      it 'save correct data' do
        post :create, section_id: @section.to_param, software: @params
        Software.last.name.should eq('hello')
      end

      it 'render new if error occured' do
        post :create, section_id: @section.to_param, software: { name: 'A' }
        response.should render_template("new")
      end
    end

    describe 'with UPDATE it should' do
      it 'update software' do
        post :update, section_id: @section.to_param,
             id: @software.to_param, software: { name: 'Lovell' }
        Software.find(@software.id).name.should eq('Lovell')
      end

      it 'redirect to show after update' do
        post :update, section_id: @section.to_param,
             id: @software.to_param, software: { tags: 'Lovell' }
        response.should redirect_to section_software_url(@section, @software)
      end

      it 'should not add a new software' do
        expect {
          post :update, section_id: @section.to_param,
               id: @software.to_param, software: { name: 'Lovell' }
        }.to change(Software, :count).by(0)
      end

      it 'render edit if error occured' do
        post :update, section_id: @section.to_param,
             id: @software.to_param, software: { name: nil }
        response.should render_template("edit")
      end
    end

    describe 'with DESTROY' do
      describe 'admins should' do
        it 'delete the software' do
          expect {
            delete :destroy, section_id: @section.to_param, id: @software.to_param
          }.to change(Software, :count).by(-1)
        end

        it 'redirect to index after delete' do
          delete :destroy, section_id: @section.to_param, id: @software.to_param
          response.should redirect_to section_path(@section)
        end
      end
    end
  end
end
