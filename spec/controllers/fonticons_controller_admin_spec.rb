require 'spec_helper'

describe FonticonsController do

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @fonticon = FactoryGirl.create(:fonticon)
  end

  describe 'as an Admin user and' do
    describe 'with INDEX' do
      before do
        get :index
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have fonticons variable' do
        assigns(:fonticons).should include(@fonticon)
      end

      it 'render without any fonticon' do
        Fonticon.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with NEW' do
      before do
        get :new
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'create a new instance of fonticon' do
        assigns(:fonticon).should be_a_new(Fonticon)
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, id: @fonticon.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'target the correct fonticon' do
        assigns(:fonticon).should eq(@fonticon)
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { icon: 'fast' }
      end

      it 'create new fonticon' do
        expect {
          post :create, fonticon: @params
        }.to change(Fonticon, :count).by(1)
      end

      it 'save correct data' do
        Fonticon.delete_all
        post :create, fonticon: @params
        Fonticon.last.icon.should eq('fast')
      end

      it 'render new if error occured' do
        post :create, fonticon: { icon: nil }
        response.should render_template("new")
      end
    end

    describe 'with UPDATE it should' do
      it 'update fonticon' do
        post :update, id: @fonticon.to_param, fonticon: { icon: 'Lovell' }
        Fonticon.last.icon.should eq('Lovell')
      end

      it 'redirect to index after update' do
        post :update, id: @fonticon.to_param, fonticon: { icon: 'Lovell' }
        response.should redirect_to fonticons_url
      end

      it 'should not add a new fonticon' do
        expect {
          post :update, id: @fonticon.to_param, fonticon: { icon: 'Lovell' }
        }.to change(Fonticon, :count).by(0)
      end

      it 'render edit if error occured' do
        post :update, id: @fonticon.to_param, fonticon: { icon: nil }
        response.should render_template("edit")
      end
    end

    describe 'with DESTROY' do
      it 'delete the fonticon' do
        expect {
          delete :destroy, id: @fonticon.to_param
        }.to change(Fonticon, :count).by(-1)
      end

      it 'redirect to index after delete' do
        delete :destroy, id: @fonticon.to_param
        response.should redirect_to(fonticons_path)
      end
    end
  end
end