require 'spec_helper'

describe FonticonsController do

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(false)
    @fonticon = FactoryGirl.create(:fonticon)
  end

  describe 'as a regular user and' do
    describe 'with INDEX' do
      before do
        get :index
      end

      it 'redirected to sections show' do
        response.should redirect_to sections_path
      end
    end

    describe 'with NEW' do
      before do
        get :new
      end

      it 'redirect to sections' do
        response.should redirect_to sections_path
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, id: @fonticon.to_param
      end

      it 'redirect to sections' do
        response.should redirect_to sections_path
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { icon: 'fast' }
      end

      it 'not creating a new record' do
        expect {
          post :create, fonticon: @params
        }.to change(Fonticon, :count).by(0)
      end

      it 'sending them to sections' do
        post :create, fonticon: @params
        response.should redirect_to sections_path
      end
    end

    describe 'with UPDATE it should' do
      it 'not update fonticon' do
        post :update, id: @fonticon.to_param, fonticon: { icon: 'Lovell' }
        Fonticon.last.icon.should_not eq('Lovell')
      end

      it 'not save if non-admin update' do
        post :update, id: @fonticon.to_param, fonticon: { icon: 'Lovell' }
        Fonticon.last.icon.should_not eq('Lovell')
      end

      it 'sending the user to section' do
        post :update, id: @fonticon.id, fonticon: { icon: 'Lovell' }
        response.should redirect_to sections_path
      end
    end

    describe 'with DESTROY' do
      it 'not deleting' do
        expect {
          delete :destroy, id: @fonticon.to_param
        }.to change(Fonticon, :count).by(0)
      end

      it 'redirect to sections' do
        delete :destroy, id: @fonticon.to_param
        response.should redirect_to sections_path
      end
    end
  end
end
