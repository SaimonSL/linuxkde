require 'spec_helper'

describe SectionsController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section)
  end

  describe 'as an Admin user and' do
    describe 'with ABOUT it should' do
      before do
        get :about
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end

      it 'render without any section' do
        Section.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with INDEX it should' do
      before do
        get :index
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end

      it 'render without any section' do
        Section.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with SHOW it should' do
      before do
        get :show, id: @section.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have section variable' do
        assigns(:section).should eq(@section)
      end

      it 'have software' do
        assigns(:section).softwares.first.should eq(@software)
      end
    end

    describe 'with NEW' do
      before do
        get :new
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'create a new instance of section' do
        assigns(:section).should be_a_new(Section)
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, id: @section.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'target the correct section' do
        assigns(:section).should eq(@section)
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { icon: 'icon-edit',
                    name: 'S',
                position: 1,
             description: 'X',
                    tags: 'L' }
      end

      it 'create new section' do
        expect {
          post :create, section: @params
        }.to change(Section, :count).by(1)
      end

      it 'save correct data' do
        post :create, section: @params
        Section.last.name.should eq('S')
      end

      it 'create new section by json post' do
        expect {
          post :create, format: :json, section: @params
        }.to change(Section, :count).by(1)
      end

      it 'render new if error occured' do
        post :create, section: { name: 'A' }
        response.should render_template("new")
      end
    end

    describe 'with UPDATE it should' do
      it 'update section' do
        post :update, id: @section.id, section: { name: 'Lovell' }
        Section.last.name.should eq('Lovell')
      end

      it 'redirect to show after update' do
        post :update, id: @section.id, section: { tags: 'Lovell' }
        response.should redirect_to section_url(@section)
      end

      it 'should not add a new section' do
        expect {
          post :update, id: @section.id, section: { name: 'Lovell' }
        }.to change(Section, :count).by(0)
      end

      it 'render edit if error occured' do
        post :update, id: @section.id, section: { name: nil }
        response.should render_template("edit")
      end
    end

    describe 'with LIST it should' do
      before do
        get :list
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end

      it 'render without any section' do
        Section.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with SORT it should' do
      before do
        Section.delete_all
        @section_first = FactoryGirl.create(:section, name: 'First', position: 1)
        @section_last = FactoryGirl.create(:section, name: 'Last', position: 99)
      end

      it 'sort the section' do
        post :sort, position: [@section_last.id, @section_first.id]
        Section.all.by_position.first.name.should eq('Last')
      end
    end

    describe 'with DESTROY' do
      it 'delete the section' do
        expect {
          delete :destroy, id: @section.id
        }.to change(Section, :count).by(-1)
      end

      it 'redirect to index after delete' do
        delete :destroy, id: @section.id
        response.should redirect_to sections_path
      end
    end
  end
end
