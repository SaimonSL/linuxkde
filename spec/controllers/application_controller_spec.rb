require 'spec_helper'

describe ApplicationController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @section = FactoryGirl.create(:section, position: 2)
    @software = FactoryGirl.create(:software, section: @section)
    @page = FactoryGirl.create(:page, software: @software)
    @instruction = FactoryGirl.create(:instruction, page: @page)
  end

  describe 'with load_sections' do
    before do
      FactoryGirl.create(:section, position: 1)
    end

    it 'should get all sections' do
      expect(subject.load_sections.count).to eq(2)
    end

    it 'should get the right order' do
      expect(subject.load_sections.last).to eq @section
    end
  end
end
