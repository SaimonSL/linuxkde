require 'spec_helper'

describe InstructionsController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section)
    @page = FactoryGirl.create(:page, software: @software)
    @instruction = FactoryGirl.create(:instruction, page: @page)
  end

  describe 'with INDEX it should' do
    before do
      get :index, section_id: @section.to_param,
                  software_id: @software.to_param,
                  page_id: @page.id
    end

    it 'be able to render' do
       response.should be_success
    end

    it 'have instructions variable' do
      assigns(:instructions).should include(@instruction)
    end

    it 'render without any instruction' do
      Instruction.delete_all
      response.should be_success
    end
  end

  describe 'with NEW' do
    describe 'admins should' do
      before do
        get :new, section_id: @section.to_param,
                  software_id: @software.to_param,
                  page_id: @page.id
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'create a new instance of instruction' do
        assigns(:instruction).should be_a_new(Instruction)
      end
    end

    describe 'non-admin should' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)
        get :new, section_id: @section.to_param,
                  software_id: @software.to_param,
                  page_id: @page.id
      end

      it 'redirect to section software page show' do
        response.should
          redirect_to section_software_path(@section, @software)
      end
    end
  end

  describe 'with EDIT' do
    describe 'admins should' do
      before do
        get :edit, section_id: @section.to_param,
                 software_id: @software.to_param,
                 page_id: @page.to_param,
                 id: @instruction.id
      end

      it 'be able to render' do
         response.should be_success
      end

      it 'target the correct instruction' do
        assigns(:instruction).should eq(@instruction)
      end
    end

    describe 'non-admin should' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)
        get :edit, section_id: @section.to_param,
                 software_id: @software.to_param,
                 page_id: @page.to_param,
                 id: @instruction.id
      end

      it 'redirected to section software page show' do
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end
    end
  end

  describe 'with CREATE it should' do
    before do
      @params = { info: 'hello', position: 1 }
    end

    it 'create new page' do
      expect {
        post :create, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page_id: @page.id,
                      instruction: @params
      }.to change(Instruction, :count).by(1)
    end

    it 'save correct data' do
      post :create, section_id: @section.to_param,
                    software_id: @software.to_param,
                    page_id: @page.id,
                    instruction: @params
      Instruction.last.info.should eq('hello')
    end

    it 'save uploaded image' do
      post :create, section_id: @section.to_param,
                    software_id: @software.to_param,
                    page_id: @page.id,
                    instruction: { info: 'hello', position: 1,
                    picture: fixture_file_upload('sample.png', 'image/png') }
      Instruction.last.picture.exists?.should eq(true)
    end

    it 'render new if error occured' do
      post :create, section_id: @section.to_param,
                    software_id: @software.to_param,
                    page_id: @page.id,
                    instruction: { info: nil }
      response.should render_template("new")
    end

    describe 'behave differently for non-admin by' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)
      end

      it 'not creating a new record' do
        expect {
          post :create, section_id: @section.to_param,
                        software_id: @software.to_param,
                        page_id: @page.id,
                        instruction: @params
        }.to change(Instruction, :count).by(0)
      end

      it 'sending them to section software page show' do
        post :create, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page_id: @page.id,
                      instruction: @params
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end
    end
  end

  describe 'with UPDATE' do
    describe 'admins should' do
      before do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page_id: @page.to_param,
                      id: @instruction.id,
                      instruction: { info: 'Lovell' }
        end

      it 'update instruction' do
        Instruction.last.info.should eq('Lovell')
      end

      it 'redirect to section software page show after update' do
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end

      it 'should not add a new instruction' do
        expect {
          post :update, section_id: @section.to_param,
                        software_id: @software.to_param,
                        page_id: @page.to_param,
                        id: @instruction.id,
                        instruction: { info: 'Lovell' }
        }.to change(Instruction, :count).by(0)
      end

      it 'render edit if error occured' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page_id: @page.to_param,
                      id: @instruction.id,
                      instruction: { info: nil }
        response.should render_template("edit")
      end
    end

    describe 'non-admins should' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page_id: @page.to_param,
                      id: @instruction.id,
                      instruction: { info: 'Lovell' }
      end

      it 'not save updates' do
        Instruction.last.info.should_not eq('Lovell')
      end

      it 'send the user to section software show page' do
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end
    end
  end

  describe 'with SORT it should' do
    before do
      Instruction.delete_all
      @instruction_first = FactoryGirl.create(:instruction, info: 'First',
                                              position: 1, page: @page)
      @instruction_last = FactoryGirl.create(:instruction, info: 'Last',
                                             position: 99, page: @page)
    end

    it 'sort the instructions' do
      post :sort, section_id: @section.to_param,
                  software_id: @software.to_param,
                  page_id: @page.id,
                  position: [@instruction_last.id, @instruction_first.id]
      @page.instructions_by_position.first.info.should eq('Last')
    end
  end

  describe 'with DESTROY IMAGE' do
    describe 'admins should' do
      before do
        @instruction.picture =
          File.open([ Rails.root, '/spec/fixtures/sample.png' ].join)

        delete :destroyimage, section_id: @section.to_param,
                             software_id: @software.to_param,
                                 page_id: @page.to_param,
                          instruction_id: @instruction.id
      end

      it 'delete the picture' do
        Instruction.last.picture.exists?.should eq(false)
      end
    end
  end

  describe 'with DESTROY' do
    describe 'admins should' do
      it 'delete the instruction' do
        expect {
          delete :destroy, section_id: @section.to_param,
                 software_id: @software.to_param,
                 page_id: @page.to_param,
                 id: @instruction.id
        }.to change(Instruction, :count).by(-1)
      end

      it 'redirect to section software page show after delete' do
        delete :destroy, section_id: @section.to_param,
               software_id: @software.to_param,
               page_id: @page.to_param,
               id: @instruction.id
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end
    end

    describe 'non-admins should' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)

      end

      it 'not deleting' do
        expect {
          delete :destroy, section_id: @section.to_param,
                 software_id: @software.to_param,
                 page_id: @page,
                 id: @instruction.to_param
                 }.to change(Instruction, :count).by(0)
      end

      it 'redirect to section software page show' do
        delete :destroy, section_id: @section.to_param,
               software_id: @software.to_param,
               page_id: @page,
               id: @instruction.to_param
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end
    end
  end
end
