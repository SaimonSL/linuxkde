require 'spec_helper'

describe PagesController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(false)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section)
    @page = FactoryGirl.create(:page, software: @software)
    @disabled_page = FactoryGirl.create(:page, software: @software, enabled: false)
    @instruction = FactoryGirl.create(:instruction, page: @page)
  end

  describe 'as a regular user and' do
    describe 'with INDEX' do
      describe 'and with pages it will' do
        before do
          get :index, section_id: @section.to_param, software_id: @software.to_param
        end

        it 'not be able to render' do
          response.should_not be_success
        end

        it 'not have pages variable' do
          assigns(:pages).should eq nil
        end
      end
    end

    describe 'with SHOW' do
      before do
        get :show, section_id: @section.to_param,
                   software_id: @software.to_param,
                   id: @disabled_page.to_param
      end

      it 'be able to render' do
        response.should redirect_to section_software_path(@section, @software)
      end
    end

    describe 'with NEW it will' do
      before do
        get :new, section_id: @section.to_param, software_id: @software.to_param
      end

      it 'be not able to render' do
        response.should redirect_to section_software_path(@section, @software)
      end
    end

    describe 'with EDIT' do
      describe 'non-admin will' do
        before do
          get :edit, section_id: @section.to_param, software_id: @software.to_param,
                     id: @page.to_param
        end

        it 'be redirecting to section software show' do
          response.should redirect_to section_software_path(@section, @software)
        end
      end
    end

    describe 'with CREATE it will' do
      before do
        @params = { name: 'hello', position: 1 }
      end

      it 'not creating a new record' do
        expect {
          post :create, section_id: @section.to_param,
                        software_id: @software.to_param,
                        page: @params
        }.to change(Page, :count).by(0)
      end

      it 'sending them to section software how' do
        post :create, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page: @params
        response.should redirect_to section_software_path(@section, @software)
      end
    end

    describe 'with UPDATE it will' do
      it 'update page' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        Page.find(@page.id).name.should_not eq('Lovell')
      end

      it 'redirect to software show after update' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end

      it 'should not add a new page' do
        expect {
          post :update, section_id: @section.to_param,
                        software_id: @software.to_param,
                        id: @page.to_param,
                        page: { name: 'Lovell' }
        }.to change(Page, :count).by(0)
      end

      it 'not save if non-admin update' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        Page.last.name.should_not eq('Lovell')
      end

      it 'sending the user to section software show page' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        response.should redirect_to section_software_path(@section, @software)
      end
    end

    describe 'with DESTROY' do
      it 'not deleting' do
        expect {
          delete :destroy, section_id: @section.to_param,
                 software_id: @software.to_param,
                 id: @page.to_param
        }.to change(Page, :count).by(0)
      end

      it 'redirect to section software show' do
        delete :destroy, section_id: @section.to_param,
               software_id: @software.to_param,
               id: @page.to_param
        response.should redirect_to section_software_path(@section, @software)
      end
    end
  end
end
