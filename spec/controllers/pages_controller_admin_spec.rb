require 'spec_helper'

describe PagesController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(true)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section)
    @page = FactoryGirl.create(:page, software: @software)
    @disabled_page = FactoryGirl.create(:page, software: @software, enabled: false)
    @instruction = FactoryGirl.create(:instruction, page: @page)
  end

  describe 'as an Admin user and' do
    describe 'with INDEX' do
      describe 'and with pages it should' do
        before do
          get :index, section_id: @section.to_param, software_id: @software.to_param
        end

        it 'be able to render' do
          response.should be_success
        end

        it 'have pages variable' do
          assigns(:pages).should include(@page)
        end
      end

      describe 'and without pages it should' do
        before do
          Page.delete_all
          get :index, section_id: @section.to_param, software_id: @software.to_param
        end

        it 'render the page' do
          response.should be_success
        end
      end
    end

    describe 'with SHOW' do
      before do
        get :show, section_id: @section.to_param,
                   software_id: @software.to_param,
                   id: @page.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have page variable' do
        assigns(:page).should eq(@page)
      end

      it 'have instructions' do
        assigns(:page).instructions_by_position.first.should eq(@instruction)
      end

      it 'have pages' do
        assigns(:pages).should include(@page)
      end

      it 'have disabled pages' do
        assigns(:pages).should include(@disabled_page)
      end
    end

    describe 'with NEW it should' do
      before do
        get :new, section_id: @section.to_param, software_id: @software.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'create a new instance of page' do
        assigns(:page).should be_a_new(Page)
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, section_id: @section.to_param, software_id: @software.to_param,
                   id: @page.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'target the correct page' do
        assigns(:page).should eq(@page)
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { name: 'hello', position: 1 }
      end

      it 'create new page' do
        expect {
          post :create, section_id: @section.to_param,
                        software_id: @software.to_param,
                        page: @params
        }.to change(Page, :count).by(1)
      end

      it 'save correct data' do
        post :create, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page: @params
        Page.last.name.should eq('hello')
      end

      it 'render new if error occured' do
        post :create, section_id: @section.to_param,
                      software_id: @software.to_param,
                      page: { name: nil }
        response.should render_template("new")
      end
    end

    describe 'with UPDATE it should' do
      it 'update page' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        Page.find(@page.id).name.should eq('Lovell')
      end

      it 'redirect to software show after update' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: 'Lovell' }
        response.should
          redirect_to section_software_page_path(@section, @software, @page)
      end

      it 'should not add a new page' do
        expect {
          post :update, section_id: @section.to_param,
                        software_id: @software.to_param,
                        id: @page.to_param,
                        page: { name: 'Lovell' }
        }.to change(Page, :count).by(0)
      end

      it 'render edit if error occured' do
        post :update, section_id: @section.to_param,
                      software_id: @software.to_param,
                      id: @page.to_param,
                      page: { name: nil }
        response.should render_template("edit")
      end
    end

    describe 'with SORT it should' do
      before do
        Page.delete_all
        @page_first = FactoryGirl.create(:page, name: 'First', position: 1, software: @software)
        @page_last = FactoryGirl.create(:page, name: 'Last', position: 99, software: @software)
      end

      it 'sort the pages' do
        post :sort, section_id: @section.to_param,
                    software_id: @software.to_param,
                    position: [@page_last.id, @page_first.id]
        @software.pages_by_position.first.name.should eq('Last')
      end
    end

    describe 'with DESTROY' do
      it 'delete the page' do
        expect {
          delete :destroy, section_id: @section.to_param,
                 software_id: @software.to_param,
                 id: @page.to_param
        }.to change(Page, :count).by(-1)
      end

      it 'redirect to section software show after delete' do
        delete :destroy, section_id: @section.to_param,
               software_id: @software.to_param,
               id: @page.to_param
        response.should redirect_to section_software_path(@section, @software)
      end
    end
  end
end
