require 'spec_helper'

describe SoftwaresController do
  stub_paperclip Software
  stub_paperclip Instruction

  let(:asset_icon) { fixture_file_upload('/sample.png', 'image/png') }

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(false)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section, enabled: true)
    @disabled_software = FactoryGirl.create(:software, section: @section,
      name: 'Not ready!', enabled: false)
    @page = FactoryGirl.create(:page, software: @software)
    @disabeld_page = FactoryGirl.create(:page, software: @software,
      name: 'not ready!', enabled: false)
    @page_for_disabled = FactoryGirl.create(:page, software: @disabled_software)
  end

  describe 'as a regular user and' do
    describe 'with INDEX' do
      before do
        get :index, section_id: @section.to_param
      end

      it 'it will not render' do
        response.should_not be_success
      end

      it 'it will not have softwares variable' do
        assigns(:softwares).should eq nil
      end
    end

    describe 'with SHOW' do
      describe 'with enabled it should' do
        before do
          get :show, section_id: @section.to_param, id: @software.to_param
        end

        it 'be able to render' do
          response.should be_success
        end

        it 'have software variable' do
          assigns(:software).should eq(@software)
        end

        it 'have page' do
          assigns(:software).pages.should include(@page)
        end

        it 'not have disabled page' do
          assigns(:software).pages.should_not include(@disabled_page)
        end
      end

      describe 'with disabled it should' do
        before do
          get :show, section_id: @section.to_param, id: @disabled_software.to_param
        end

        it 'redirect to section index' do
          response.should redirect_to section_path(@section)
        end
      end
    end

    describe 'with NEW' do
      before do
        get :new, section_id: @section.to_param
      end

      it 'be redirected to section show' do
        response.should redirect_to section_path(@section)
      end
    end

    describe 'with EDIT' do
      before do
        get :edit, section_id: @section.to_param, id: @software.to_param
      end

      it 'be redirected section show' do
        response.should redirect_to section_path(@section)
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { name: 'hello', description: 'X', tags: 'L', icon: asset_icon }
      end

      it 'not creating a new record' do
        expect {
          post :create, section_id: @section.to_param, software: @params
        }.to change(Software, :count).by(0)
      end

      it 'sending them to section show' do
        post :create, section_id: @section.to_param, software: @params
        response.should redirect_to section_path(@section)
      end
    end

    describe 'with UPDATE it should' do
      it 'not save if non-admin update' do
        post :update, section_id: @section.to_param,
             id: @software.to_param, software: { tags: 'Lovell' }
        Software.last.tags.should_not eq('Lovell')
      end

      it 'sending the user to section show page' do
        post :update, section_id: @section.to_param,
             id: @software.to_param, software: { tags: 'Lovell' }
        response.should redirect_to section_path(@section)
      end
    end

    describe 'with DESTROY' do
      it 'not deleting' do
        expect {
          delete :destroy, section_id: @section.to_param, id: @software.to_param
        }.to change(Software, :count).by(0)
      end

      it 'redirect to section show' do
        delete :destroy, section_id: @section.to_param, id: @software.to_param
        response.should redirect_to section_path(@section)
      end
    end
  end
end
