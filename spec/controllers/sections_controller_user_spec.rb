require 'spec_helper'

describe SectionsController do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    ApplicationController.any_instance.stub(:is_admin?).and_return(false)
    @section = FactoryGirl.create(:section)
    @software = FactoryGirl.create(:software, section: @section)
  end

  describe 'as a regular user and' do
    describe 'with ABOUT it should' do
      before do
        get :about
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end

      it 'render without any section' do
        Section.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with INDEX it should' do
      before do
        get :index
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end

      it 'render without any section' do
        Section.delete_all
        get :index
        response.should be_success
      end
    end

    describe 'with SHOW it should' do
      before do
        get :show, id: @section.to_param
      end

      it 'be able to render' do
        response.should be_success
      end

      it 'have section variable' do
        assigns(:section).should eq(@section)
      end

      it 'have software' do
        assigns(:section).softwares.first.should eq(@software)
      end
    end

    describe 'with NEW' do
      before do
        get :new
      end

      it 'redirect to index' do
        response.should redirect_to sections_path
      end
    end

    describe 'with EDIT' do
      before do
        ApplicationController.any_instance.stub(:is_admin?).and_return(false)
        get :edit, id: @section.to_param
      end

      it 'redirect to index' do
        response.should redirect_to sections_path
      end
    end

    describe 'with CREATE it should' do
      before do
        @params = { icon: 'icon-edit',
                    name: 'S',
                position: 1,
             description: 'X',
                    tags: 'L' }
      end

      it 'not creating a new record' do
        expect {
          post :create, section: @params
        }.to change(Section, :count).by(0)
      end

      it 'sending them to index' do
        post :create, section: @params
        response.should redirect_to sections_path
      end
    end

    describe 'with UPDATE it should' do
      it 'not save if non-admin update' do
        post :update, id: @section.id, section: { tags: 'Lovell' }
        Section.last.tags.should_not eq('Lovell')
      end

      it 'sending the user to index page' do
        post :update, id: @section.id, section: { tags: 'Lovell' }
        response.should redirect_to sections_path
      end
    end

    describe 'with LIST it should' do
      before do
        get :list
      end

      it 'not be able to render' do
        response.should_not be_success
      end

      it 'sending the user to index page' do
        response.should redirect_to sections_path
      end

      it 'have sections variable' do
        assigns(:sections).should include(@section)
      end
    end

    describe 'with DESTROY' do
      it 'not deleting' do
        expect {
          delete :destroy, id: @section.id
        }.to change(Section, :count).by(0)
      end

      it 'redirect to index' do
        delete :destroy, id: @section.id
        response.should redirect_to sections_path
      end
    end
  end
end