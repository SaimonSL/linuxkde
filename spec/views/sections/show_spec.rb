require 'spec_helper'

describe 'with section show page' do
  stub_paperclip Software
  stub_paperclip Instruction

  before do
    @section =   FactoryGirl.create(:section)
    software =  FactoryGirl.create(:software, section: @section)
    FactoryGirl.create(:page, software: software)
  end

  describe 'and admins viewing' do
    before do
      ApplicationController.any_instance.stub(:is_admin?).and_return(true)
      visit section_path(@section)
    end

    describe 'they should see a' do
      it 'Add New Software button' do
        page.should have_content 'Add New Software'
      end

      it 'Edit Section button' do
        page.should have_content 'Edit Section'
      end

      it 'Delete Section button' do
        page.should have_content 'Delete Section'
      end

      it 'Add New Page button' do
        page.should have_content 'Add New Page'
      end

      it 'Edit Software button' do
        page.should have_content 'Edit Software'
      end

      it 'Delete Software button' do
        page.should have_content 'Delete Software'
      end

      it 'List Pages button' do
        page.should have_content 'List Pages'
      end
    end
  end

  describe 'and NON-admins viewing' do
    before do
      ApplicationController.any_instance.stub(:is_admin?).and_return(false)
      visit section_path(@section)
    end

    describe 'they should NOT see a' do
      it 'Add New Software button' do
        page.should_not have_content 'Add New Software'
      end

      it 'Edit Section button' do
        page.should_not have_content 'Edit Section'
      end

      it 'Delete Section button' do
        page.should_not have_content 'Delete Section'
      end

      it 'Add New Page button' do
        page.should_not have_content 'Add New Page'
      end

      it 'Edit Software button' do
        page.should_not have_content 'Edit Software'
      end

      it 'Delete Software button' do
        page.should_not have_content 'Delete Software'
      end

      it 'List Pages button' do
        page.should_not have_content 'List Pages'
      end
    end
  end
end
