Linuxkde::Application.routes.draw do
  resources :visitors
  resources :fonticons

  resources :sections do
    post :sort, on: :collection
    get :list, on: :collection
    get :about, on: :collection

    resources :softwares do
      resources :pages do
        post :sort, on: :collection
        resources :instructions do
          post :sort, on: :collection
          delete :destroyimage
        end
      end
    end
  end

  root to: 'sections#index'
end
