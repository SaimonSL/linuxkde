if Rails.env.development? || Rails.env.test?
  class CLOGGER
    @log_file = 'log/custom.log'
    @debug = Logger.new(@log_file)

    def self.info(message)
      self.prepare
      @debug.info message
    end

    def self.debug(message)
      self.prepare
      @debug.debug message
    end

    def self.error(message)
      self.prepare
      @debug.error message
    end

    def self.raw(message)
      self.raw_format
      @debug.info message
    end

    def self.new_line
      self.clean_format
      @debug.info "\n"
    end

    def self.new_section(title)
      @debug.info "\n\n -------- #{title} --------"
    end

    def self.clear
      File.open(@log_file, 'w') {}
      "Log file [#{@log_file}] cleared."
    end

    def self.delete
      File.delete(@log_file) if File.exist?(@log_file)
    end

    def self.log_to(log_file)
      @log_file = "log/#{log_file}"
      @debug = Logger.new(@log_file)
    end

    def self.log_file
      @log_file
    end

    def self.to_s
      IO.readlines(@log_file)[-10..-1]
    end

    private
    def self.prepare
      @debug = Logger.new(@log_file) if File.exist?(@log_file)
      @debug.formatter = proc do |severity, datetime, progname, msg|
        "#{datetime.strftime("%l:%M:%S %p")} [#{severity}]: #{msg}\n"
      end
    end

    def self.clean_format
      @debug = Logger.new(@log_file) if File.exist?(@log_file)
      @debug.formatter = proc do |severity, datetime, progname, msg|
        "\n"
      end
    end

    def self.raw_format
      @debug = Logger.new(@log_file) if File.exist?(@log_file)
      @debug.formatter = proc do |severity, datetime, progname, msg|
        "\n#{msg}\n"
      end
    end
  end
end
