# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20131007224144) do

  create_table "fonticons", force: true do |t|
    t.string   "icon",       limit: 32
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "instructions", force: true do |t|
    t.integer  "page_id"
    t.integer  "position"
    t.text     "info"
    t.string   "picture_file_name"
    t.string   "picture_content_type"
    t.integer  "picture_file_size"
    t.datetime "picture_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "instructions", ["position"], name: "index_instructions_on_position", using: :btree

  create_table "pages", force: true do |t|
    t.integer  "software_id"
    t.integer  "position"
    t.string   "name",        limit: 128
    t.boolean  "enabled",                 default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "pages", ["position"], name: "index_pages_on_position", using: :btree

  create_table "sections", force: true do |t|
    t.string   "icon",        limit: 24
    t.string   "name",        limit: 32
    t.integer  "position"
    t.string   "description"
    t.string   "tags"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sections", ["position"], name: "index_sections_on_position", using: :btree

  create_table "softwares", force: true do |t|
    t.integer  "section_id"
    t.string   "name",              limit: 64
    t.string   "icon_file_name"
    t.string   "icon_content_type"
    t.integer  "icon_file_size"
    t.datetime "icon_updated_at"
    t.string   "description"
    t.boolean  "enabled",                      default: false
    t.string   "tags"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "visitors", force: true do |t|
    t.string   "ip",          limit: 16
    t.integer  "section_id"
    t.integer  "software_id"
    t.integer  "page_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
