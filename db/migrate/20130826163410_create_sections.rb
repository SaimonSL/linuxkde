class CreateSections < ActiveRecord::Migration
  def change
    create_table :sections do |t|
      t.string :icon, limit: 24
      t.string :name, limit: 32
      t.integer :position
      t.string :description
      t.string :tags

      t.timestamps
    end

    add_index :sections, :position
  end
end
