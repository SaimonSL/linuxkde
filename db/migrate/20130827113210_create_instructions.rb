class CreateInstructions < ActiveRecord::Migration
  def change
    create_table :instructions do |t|
      t.integer :page_id
      t.integer :position
      t.text :info
      t.attachment :picture

      t.timestamps
    end
    add_index :instructions, :position
  end
end
