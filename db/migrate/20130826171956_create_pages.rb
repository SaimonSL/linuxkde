class CreatePages < ActiveRecord::Migration
  def change
    create_table :pages do |t|
      t.integer :software_id
      t.integer :position
      t.string :name, limit: 128
      t.boolean :enabled, default: false

      t.timestamps
    end

    add_index :pages, :position
  end
end
