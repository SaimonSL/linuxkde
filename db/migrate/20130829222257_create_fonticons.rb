class CreateFonticons < ActiveRecord::Migration
  def change
    create_table :fonticons do |t|
      t.string :icon, limit: 32

      t.timestamps
    end
  end
end
