class CreateSoftwares < ActiveRecord::Migration
  def change
    create_table :softwares do |t|
      t.integer :section_id
      t.string :name, limit: 64
      t.attachment :icon
      t.string :description
      t.boolean :enabled, default: false
      t.string :tags

      t.timestamps
    end
  end
end
