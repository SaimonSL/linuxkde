class CreateVisitors < ActiveRecord::Migration
  def change
    create_table :visitors do |t|
      t.string :ip, limit: 16
      t.integer :section_id
      t.integer :software_id
      t.integer :page_id

      t.timestamps
    end
  end
end
