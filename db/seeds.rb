# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Section.delete_all
Section.create icon: 'fa edit', name: 'Getting Started', position: 1,
               description: 'Learn what to do once you have instelled the os',
               tags: 'install, update'
